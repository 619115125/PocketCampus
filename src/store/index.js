
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 个人信息
    personInfo: {},
    count: {
      scCount: 0,
      applyCount: 0,
      msgCount: 0,
      count: 0
    },
    backPath: ''
  },
  getters: {
    personInfo: state => state.personInfo,
    count: state => state.count,
    backPath: state => state.backPath
  },
  mutations: {
    changePersonInfo (state, personInfo) {
      state.personInfo = personInfo
    },
    changeCount (state, count) {
      state.count = count
    },
    changeBackPath (state, path) {
      state.backPath = path
    }
  },
  actions: {},
  modules: {}
})

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import './assets/stylus/index.styl'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import {Tag, Button, Input, Table, TableColumn, Select, Option} from 'element-ui'
import 'font-awesome/css/font-awesome.css'
import KeenUI from 'keen-ui'
import 'keen-ui/dist/keen-ui.css'
import App from './App'
import router from './router'
import store from './store'
Vue.prototype.$ELEMENT = { size: 'small' }
Vue.use(Tag)
Vue.use(Button)
Vue.use(Input)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Select)
Vue.use(Option)

Vue.use(MintUI)
Vue.use(KeenUI)

Vue.config.productionTip = false
Vue.prototype.$http = axios

Vue.directive('metaTitle', {
  update: function (el, binding) {
    document.title = binding.value
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})

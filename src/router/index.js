import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from '@/api/axiosApi'

// 路由懒加载，需要用的时候才调用，避免第一个页面加载时间过长
const Home = () => import('@/components/home/home')
const Person = () => import('@/components/person/person')
const Consultation = () => import('@/components/consultation/consultation')

const HomeDetail = () => import('@/components/home/homeDetail')
const ConsultationDetail = () => import('@/components/consultation/consultationDetail')
const EnterList = () => import('@/components/person/enterList')
const MessageList = () => import('@/components/person/messageList')

const Login = () => import('@/components/login/login')

const AboutQD = () => import('@/components/aboutQD')
const Feedback = () => import('@/components/feedback')

Vue.use(VueRouter)

const router = new VueRouter({
  // 默认hash模式，带有#号的
  // mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/wechat/consultation'
    },
    {
      path: '/wechat',
      redirect: '/wechat/consultation'
    },
    {
      path: '/wechat/home',
      name: 'home',
      component: Home
    },
    {
      path: '/wechat/homeDetail/:type/:contentId',
      name: 'homeDetail',
      component: HomeDetail
    },
    {
      path: '/wechat/consultation',
      name: 'consultation',
      component: Consultation
    },
    {
      path: '/wechat/consultationDetail/:contentId',
      name: 'consultationDetail',
      component: ConsultationDetail
    },
    {
      path: '/wechat/person',
      name: 'person',
      component: Person
    },
    {
      path: '/wechat/enterList/:category',
      name: 'enterList',
      component: EnterList
    },
    {
      path: '/wechat/message/:status',
      component: MessageList
    },
    {
      path: '/wechat/about',
      component: AboutQD
    },
    {
      path: '/wechat/feedback',
      component: Feedback
    },
    {
      path: '/wechat/login',
      component: Login
    },
    {
      path: '/wechat/snacks',
      component: () => import('@/components/snacks/snacks')
    },
    {
      path: '/wechat/snacks/manage',
      component: () => import('@/components/snacks/manage')
    }
  ]
})

// 路由导航钩子（在路由跳转之前处理）
router.beforeEach((to, from, next) => {
  // URL解析 微回调地址中code存在 ?....# 中
  if (document.URL.match(/\?.*#/)) {
    let temp = document.URL.match(/\?.*#/)[0]
    if (temp.match(/=.*&/)) {
      // 解析Code
      let code = temp.match(/=.*&/)[0]
      let state = temp.match(/state=.*#/)[0]
      code = code.substr(1, code.length) // 去掉 ?
      code = code.substr(0, code.length - 1)  // 去掉 #
      // 微信重定向过来的时候
      const salesCode = to.query.salesCode ? to.query.salesCode : ''
      let url = '/wxLogin/callBack'
      if (state.indexOf('QQ') > -1) {
        url = '/login/qq'
      }
      // 通过Code请求获取openId
      axios.post(url, { code: code, sourceCode: salesCode }, (data) => {
        if (data.openId && data.openId !== null) {
          localStorage.setItem('openId', data.openId)
        }
        if (data.salesCode && data.salesCode !== null) {
          to.query.salesCode = data.salesCode
        }
      })
      // 重置URL
      window.history.replaceState({}, 0, document.URL.replace(temp.substr(0, temp.length - 1), ''))
    }
  }
  if (to.name === 'consultationDetail') {
    let child = document.getElementById('changyan_mobile_js')
    if (child === null) {
      let node = document.createElement('script')
      node.id = 'changyan_mobile_js'
      node.charset = 'utf-8'
      node.type = 'text/javascript'
      node.src = 'https://changyan.sohu.com/upload/mobile/wap-js/changyan_mobile.js?client_id=cytlC7JFC&conf=prod_b25c9e894908d3993b63a57772926cc9'
      document.body.appendChild(node)
    } else {
      window.history.go(0)
    }
  }
  const backPath = localStorage.getItem('backPath')
  if (backPath !== null && to.path !== '/wechat/login') {
    localStorage.removeItem('backPath')
    next(backPath)
  } else {
    next()
  }
})
export default router

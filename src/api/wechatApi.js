import { REDIRECT_URI, APP_ID, SCOPE } from '@/api/config'

export default {
  wechatLogin: (url = null) => {
    let tempUrl = REDIRECT_URI
    if (url !== null) {
      tempUrl = url
    }
    window.location.href = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' + APP_ID + '&redirect_uri=' + encodeURIComponent(tempUrl) + '&response_type=code&scope=' + SCOPE + '&state=STATE#wechat_redirect'
  }
}


export const BASE_URL = 'http://www.qdcollege.cn/api'
// export const BASE_URL = 'http://192.168.1.115:8080/api'

// 微信相关参数
export const REDIRECT_URI = 'http://www.qdcollege.cn/#/wechat/consultation'
// export const REDIRECT_URI = 'http://develop.qdcollege.cn/#/wechat/consultation'

// 微信参数
export const APP_ID = 'wx4e93a336aad05a44'
export const SCOPE = 'snsapi_userinfo'

// QQ 参数
